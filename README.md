Transaction Summary
================================================================================
Upgrade  7 Packages

Total download size: 6.4 M
Is this ok [y/d/N]: yes
Downloading packages:
(1/2): packages/7/x86_64/prestodelta                       | 184 kB   00:05     
(2/2): updates/7/x86_64/prestodelta                        | 189 kB   00:09     
Delta RPMs reduced 6.2 M of updates to 2.3 M (62% saved)
(1/7): systemd-libs-219-62.el7_219-62.el7_6.2.x86_64.drpm  | 121 kB   00:06     
(2/7): systemd-python-219-62.el7_219-62.el7_6.2.x86_64.drp |  91 kB   00:06     
(3/7): libgudev1-219-62.el7_6.2.x86_64.rpm                 |  96 kB   00:00     
(4/7): tzdata-java-2018g-1.el7_2018i-1.el7.noarch.drpm     | 102 kB   00:07     
(5/7): systemd-sysv-219-62.el7_6.2.x86_64.rpm              |  83 kB   00:00     
(6/7): tzdata-2018g-1.el7_2018i-1.el7.noarch.drpm          | 308 kB   00:07     
(7/7): systemd-219-62.el7_219-62.el7_6.2.x86_64.drpm       | 1.7 MB   00:07     
Finishing delta rebuilds of 5 package(s) (6.2 M)
--------------------------------------------------------------------------------
Total                                               58 kB/s | 2.5 MB  00:44     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : systemd-libs-219-62.el7_6.2.x86_64                          1/14 
  Updating   : systemd-219-62.el7_6.2.x86_64                               2/14 
  Updating   : systemd-python-219-62.el7_6.2.x86_64                        3/14 
  Updating   : systemd-sysv-219-62.el7_6.2.x86_64                          4/14 
  Updating   : libgudev1-219-62.el7_6.2.x86_64                             5/14 
  Updating   : tzdata-java-2018i-1.el7.noarch                              6/14 
  Updating   : tzdata-2018i-1.el7.noarch                                   7/14 
  Cleanup    : systemd-sysv-219-62.el7.x86_64                              8/14 
  Cleanup    : tzdata-java-2018g-1.el7.noarch                              9/14 
  Cleanup    : tzdata-2018g-1.el7.noarch                                  10/14 
  Cleanup    : systemd-python-219-62.el7.x86_64                           11/14 
  Cleanup    : systemd-219-62.el7.x86_64                                  12/14 
  Cleanup    : libgudev1-219-62.el7.x86_64                                13/14 
  Cleanup    : systemd-libs-219-62.el7.x86_64                             14/14 
  Verifying  : tzdata-2018i-1.el7.noarch                                   1/14 
  Verifying  : systemd-python-219-62.el7_6.2.x86_64                        2/14 
  Verifying  : libgudev1-219-62.el7_6.2.x86_64                             3/14 
  Verifying  : systemd-sysv-219-62.el7_6.2.x86_64                          4/14 
  Verifying  : systemd-libs-219-62.el7_6.2.x86_64                          5/14 
  Verifying  : tzdata-java-2018i-1.el7.noarch                              6/14 
  Verifying  : systemd-219-62.el7_6.2.x86_64                               7/14 
  Verifying  : tzdata-2018g-1.el7.noarch                                   8/14 
  Verifying  : systemd-libs-219-62.el7.x86_64                              9/14 
  Verifying  : systemd-219-62.el7.x86_64                                  10/14 
  Verifying  : systemd-sysv-219-62.el7.x86_64                             11/14 
  Verifying  : tzdata-java-2018g-1.el7.noarch                             12/14 
  Verifying  : systemd-python-219-62.el7.x86_64                           13/14 
  Verifying  : libgudev1-219-62.el7.x86_64                                14/14 

Updated:
  libgudev1.x86_64 0:219-62.el7_6.2     systemd.x86_64 0:219-62.el7_6.2        
  systemd-libs.x86_64 0:219-62.el7_6.2  systemd-python.x86_64 0:219-62.el7_6.2 
  systemd-sysv.x86_64 0:219-62.el7_6.2  tzdata.noarch 0:2018i-1.el7            
  tzdata-java.noarch 0:2018i-1.el7     

Complete!
[root@assets ~]# 